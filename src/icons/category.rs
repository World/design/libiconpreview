use gettextrs::gettext;
use glib::subclass::prelude::*;
use gtk::{gdk, gio, glib, prelude::*};

use super::Icon;
use crate::icons::utils::FORBIDDEN_ICONS;

mod imp {
    use std::{
        cell::{Cell, OnceCell, RefCell},
        collections::VecDeque,
    };

    use super::*;

    #[derive(Default, glib::Properties)]
    #[properties(wrapper_type = super::Category)]
    pub struct Category {
        #[property(get, set, construct_only)]
        pub name: OnceCell<String>,
        #[property(get, set, construct_only)]
        pub icons: OnceCell<gio::ListStore>,
        #[property(get, set)]
        pub is_rendered: Cell<bool>,
        #[property(get, set)]
        pub is_system: Cell<bool>,
        pub queue: RefCell<VecDeque<Icon>>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for Category {
        const NAME: &'static str = "Category";
        type Type = super::Category;
    }

    #[glib::derived_properties]
    impl ObjectImpl for Category {}
}

glib::wrapper! {
    pub struct Category(ObjectSubclass<imp::Category>);
}

impl Category {
    pub fn new(name: &str) -> Self {
        glib::Object::builder()
            .property("name", name)
            .property("icons", gio::ListStore::new::<Icon>())
            .build()
    }

    pub fn splice(&self, icons: &[Icon]) {
        self.icons().splice(0, 0, icons);
    }

    pub(crate) fn splice_queue(&self, icons: Vec<Icon>) {
        self.imp().queue.replace(icons.into());
    }

    pub async fn queue_rendering(&self) {
        if self.is_rendered() {
            return;
        }
        tracing::debug!("Queuing rendering of {}", self.name());
        {
            let icons = self.imp().queue.borrow().clone();
            let Some(icon) = icons.front() else {
                return;
            };
            let handle = icon.imp().handle.get().unwrap().clone();
            let renderer: rsvg::CairoRenderer = rsvg::CairoRenderer::new(&handle);
            for icon in icons.iter() {
                match icon.render(&renderer, None).await {
                    Ok(_) => {
                        self.append(icon);
                    }
                    Err(err) => {
                        tracing::error!("Failed to render icon {err}");
                    }
                }
            }
        };
        self.imp().queue.borrow_mut().clear();
        self.set_is_rendered(true);
    }

    pub fn append(&self, icon: &Icon) {
        self.icons().append(icon);
    }

    pub fn new_system(app_id: &str) -> anyhow::Result<Self> {
        // Load default theme icons
        let theme = gtk::IconTheme::for_display(&gdk::Display::default().unwrap());
        let gtk_group = Self::new(&gettext("Pre-Installed System Icons"));
        // It is a system group
        gtk_group.set_is_system(true);
        gtk_group.set_is_rendered(true);

        let icons = theme
            .icon_names()
            .into_iter()
            .filter(|icon_name| {
                // filter out unneeded icons
                !FORBIDDEN_ICONS.contains(&icon_name.as_str())
                    && icon_name.ends_with("-symbolic")
                    && icon_name != &format!("{app_id}-symbolic")
            })
            .collect::<Vec<glib::GString>>();

        gtk_group.splice(
            &icons
                .into_iter()
                .map(|icon_name| Icon::new(&icon_name, vec![], true, None))
                .collect::<Vec<Icon>>(),
        );

        Ok(gtk_group)
    }
}
