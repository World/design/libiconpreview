use std::{path::PathBuf, rc::Rc};

use anyhow::Result;
use glib::subclass::prelude::*;
use gtk::{gdk, gio, glib, gsk, prelude::*};
use rsvg::{CairoRenderer, SvgHandle};

use crate::icons::utils::{clean_svg, replace_fill_with_classes};

mod imp {
    use std::cell::{Cell, OnceCell, RefCell};

    use super::*;

    #[derive(Default, glib::Properties)]
    #[properties(wrapper_type = super::Icon)]
    pub struct Icon {
        #[property(get, set, construct_only)]
        pub name: OnceCell<String>,
        #[property(get, set)]
        pub tags: RefCell<Vec<String>>,
        #[property(get, set, construct_only)]
        pub is_system: Cell<bool>,
        #[property(get, set, construct_only)]
        pub is_extern: Cell<bool>,
        #[property(get, set, construct_only)]
        pub id: OnceCell<Option<String>>,
        #[property(get, set, construct_only)]
        pub rect: OnceCell<Option<String>>,
        pub cache_file: OnceCell<PathBuf>,
        pub handle: OnceCell<Rc<SvgHandle>>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for Icon {
        const NAME: &'static str = "Icon";
        type Type = super::Icon;
    }

    #[glib::derived_properties]
    impl ObjectImpl for Icon {}
}

glib::wrapper! {
    pub struct Icon(ObjectSubclass<imp::Icon>);
}

unsafe impl Send for Icon {}
unsafe impl Sync for Icon {}

impl Icon {
    pub fn new(
        name: &str,
        tags: Vec<String>,
        is_system: bool,
        cache_file: Option<PathBuf>,
    ) -> Self {
        let obj = glib::Object::builder::<Self>()
            .property("name", name)
            .property("tags", tags)
            .property("is-system", is_system)
            .property("is-extern", false)
            .build();
        if let Some(cache_file) = cache_file {
            let imp = obj.imp();
            imp.cache_file.set(cache_file).unwrap();
        }
        obj
    }

    pub(crate) fn new_unparsed(
        handle: Rc<SvgHandle>,
        name: String,
        id: String,
        rect: Option<String>,
        tags: Vec<String>,
        cache_file: PathBuf,
    ) -> Self {
        let obj = glib::Object::builder::<Self>()
            .property("id", id)
            .property("name", name)
            .property("rect", rect)
            .property("tags", tags)
            .property("is-system", false)
            .property("is-extern", true)
            .build();
        let imp = obj.imp();
        imp.cache_file.set(cache_file).unwrap();
        let _ = imp.handle.set(handle);
        obj
    }

    pub fn cache(&self) -> PathBuf {
        self.imp().cache_file.get().unwrap().to_owned()
    }

    pub async fn render(
        &self,
        renderer: &CairoRenderer<'_>,
        into: Option<gio::File>,
    ) -> Result<()> {
        // Nothing to render
        if !self.is_extern() {
            return Ok(());
        }
        let cache = match into {
            Some(file) => file,
            None => self.file(),
        };
        let id = format!("#{}", self.rect().unwrap_or(self.id().unwrap()));

        let viewport = {
            let dimensions = renderer.intrinsic_dimensions();

            cairo::Rectangle::new(0.0, 0.0, dimensions.width.length, dimensions.height.length)
        };
        let (rect, _) = renderer.geometry_for_layer(Some(&id), &viewport)?;

        let tmp_file = gio::File::new_tmp(None::<std::path::PathBuf>)?.0;
        let mut surface =
            cairo::SvgSurface::new(rect.width(), rect.height(), Some(tmp_file.path().unwrap()))
                .unwrap();
        surface.set_document_unit(cairo::SvgUnit::Px);
        let cr = cairo::Context::new(&surface)?;
        cr.translate(-rect.x(), -rect.y());

        renderer.render_layer(&cr, None, &viewport)?;
        surface.finish();
        let bytes = tmp_file.load_contents_future().await?.0;
        let xml_input = String::from_utf8_lossy(bytes.as_ref()).to_string();
        if xml_input.is_empty() {
            return Ok(());
        }
        let xml_output = clean_svg(&replace_fill_with_classes(&xml_input)?)?;
        cache
            .replace_contents_future(
                xml_output,
                None,
                false,
                gio::FileCreateFlags::REPLACE_DESTINATION,
            )
            .await
            .map_err(|e| e.1)?;
        tmp_file.delete_future(glib::Priority::default()).await?;
        Ok(())
    }

    pub fn should_display(&self, search_str: &str) -> bool {
        let icon_name = self.name().to_lowercase();
        let mut icon_terms = icon_name.split('-');
        // Check if the icon should be shown for the searched string
        let mut found_tags = self.tags();
        found_tags.retain(|tag| tag.to_lowercase().contains(search_str));

        icon_terms.any(|term| term.contains(search_str))
            || !found_tags.is_empty()
            || icon_name.contains(search_str)
    }

    pub async fn save(&self, destination: gio::File) -> Result<()> {
        let icon_file = gio::File::for_path(self.cache());
        icon_file
            .copy_future(
                &destination,
                gio::FileCopyFlags::OVERWRITE,
                glib::Priority::default(),
            )
            .0
            .await?;
        Ok(())
    }

    pub async fn derive(&self) -> anyhow::Result<()> {
        let icon_file = gio::File::for_path(self.cache());
        // Push the icon into /tmp so other apps can access it for now. Not ideal :(
        let mut dest = std::env::temp_dir();
        dest.push(icon_file.basename().unwrap());
        let dest = gio::File::for_path(dest);
        let uri = dest.uri();
        self.save(dest).await?;

        gio::AppInfo::launch_default_for_uri_future(&uri, gio::AppLaunchContext::NONE).await?;
        Ok(())
    }

    pub async fn copy(&self) -> Result<()> {
        let display = gdk::Display::default().unwrap();
        let clipboard = display.clipboard();

        let icon_file = gio::File::for_path(self.cache());
        let (bytes, _) = icon_file.load_bytes_future().await?;

        let content = gdk::ContentProvider::for_bytes("image/svg+xml", &bytes);
        clipboard.set_content(Some(&content))?;
        Ok(())
    }

    pub fn copy_name(&self) {
        if let Some(display) = gdk::Display::default() {
            let clipboard = display.clipboard();
            clipboard.set_text(&self.name());
        }
    }
    pub fn file(&self) -> gio::File {
        if !self.is_system() {
            gio::File::for_path(self.cache())
        } else {
            unreachable!()
        }
    }
    pub fn paintable(&self, size: i32) -> gtk::IconPaintable {
        if self.is_system() {
            let display = gdk::Display::default().unwrap();
            let theme = gtk::IconTheme::for_display(&display);
            theme.lookup_icon(
                &self.name(),
                &[],
                size,
                1,
                gtk::TextDirection::Ltr,
                gtk::IconLookupFlags::FORCE_SYMBOLIC,
            )
        } else {
            gtk::IconPaintable::for_file(&self.file(), size, 1)
        }
    }

    // Returns a texture for use in GNOME Shell.
    pub fn texture(&self) -> Option<gdk::Texture> {
        // The shells wants them at 24px, but we draw them at 48 for hdpi
        // monitors.
        const SIZE: i32 = 48;
        let snapshot = gtk::Snapshot::new();
        let paintable = self.paintable(SIZE);
        // Colors displayed by gtk/adw in dark mode.
        let fg_color = "#ffffff"; // Light 1
        let error_color = "#ff7b63";
        let warning_color = "#f8e45c"; // Yellow 2
        let success_color = "#8ff0a4"; // Green 1

        let colors = [
            gdk::RGBA::parse(fg_color).unwrap(),
            gdk::RGBA::parse(error_color).unwrap(),
            gdk::RGBA::parse(warning_color).unwrap(),
            gdk::RGBA::parse(success_color).unwrap(),
        ];

        // The renderer was created up in the air so there is no scale factor
        // and the texture will be drawn at size `SIZE` and later stored as a
        // png of size `SIZE`.
        let renderer = gsk::GLRenderer::new();
        renderer.realize(gdk::Surface::NONE).ok()?;
        paintable.snapshot_symbolic(&snapshot, SIZE.into(), SIZE.into(), &colors);
        let node = snapshot.to_node()?;
        let texture = renderer.render_texture(&node, None);
        renderer.unrealize();

        Some(texture)
    }
}
