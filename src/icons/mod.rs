pub(crate) mod category;
pub(crate) mod icon;
pub(crate) mod icons_set;
pub(crate) mod utils;

pub use category::Category;
pub use icon::Icon;
pub use icons_set::IconsSet;
pub use utils::*;

pub static SYMBOLIC_CSS: &str = r#"
.error{fill:#e01b24 !important;}
.success{fill:#33d17a !important;}
.warning{fill:#ff7800 !important;}
"#;
