pub mod icons;
mod project;
mod utils;
pub mod widgets;

pub use project::{Project, ProjectType};
