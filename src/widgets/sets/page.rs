use adw::{prelude::*, subclass::prelude::*};
use gtk::{
    gio,
    glib::{self, clone},
};

use crate::{widgets::sets::IconsSetView, Project, ProjectType};

#[derive(Default, Clone, Copy, Eq, PartialEq)]
pub enum View {
    #[default]
    Empty,
    Icons,
}

mod imp {
    use std::{
        cell::{Cell, RefCell},
        rc::Rc,
    };

    use super::*;

    #[derive(Default, gtk::CompositeTemplate)]
    #[template(file = "../../../data/ui/icons_set_page.ui")]
    pub struct IconsSetPage {
        pub(super) current_view: Cell<View>,
        #[template_child]
        pub(super) headerbar: TemplateChild<adw::HeaderBar>,
        #[template_child]
        pub(super) icons_set_view: TemplateChild<IconsSetView>,
        #[template_child]
        pub(super) progress: TemplateChild<gtk::ProgressBar>,
        pub(super) open_project: Rc<RefCell<Option<Project>>>,
        #[template_child]
        pub(super) revealer: TemplateChild<gtk::Revealer>,
        #[template_child]
        pub(super) stack: TemplateChild<gtk::Stack>,
        #[template_child]
        pub(super) search_entry: TemplateChild<gtk::SearchEntry>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for IconsSetPage {
        const NAME: &'static str = "IconsSetPage";
        type Type = super::IconsSetPage;
        type ParentType = adw::Bin;
        type Interfaces = (gtk::Buildable,);

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
            klass.bind_template_instance_callbacks();
            klass.install_action("page.search", None, |widget, _, _| {
                widget.imp().search_entry.grab_focus();
            });
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for IconsSetPage {
        fn constructed(&self) {
            self.parent_constructed();

            let obj = self.obj();
            // Search
            self.search_entry.set_key_capture_widget(Some(&*obj));

            obj.set_view(View::Empty);
        }
    }

    impl WidgetImpl for IconsSetPage {}
    impl BinImpl for IconsSetPage {}
    impl BuildableImpl for IconsSetPage {
        fn add_child(&self, builder: &gtk::Builder, child: &glib::Object, type_: Option<&str>) {
            match type_ {
                Some("header-start") => {
                    self.headerbar
                        .pack_start(child.downcast_ref::<gtk::Widget>().unwrap());
                }
                Some("header-end") => {
                    self.headerbar
                        .pack_end(child.downcast_ref::<gtk::Widget>().unwrap());
                }
                _ => self.parent_add_child(builder, child, type_),
            }
        }
    }
}

glib::wrapper! {
    pub struct IconsSetPage(ObjectSubclass<imp::IconsSetPage>)
        @extends gtk::Widget, adw::Bin,
        @implements gtk::Buildable;
}

#[gtk::template_callbacks]
impl IconsSetPage {
    pub fn open_project(&self) -> Option<Project> {
        self.imp().open_project.borrow().clone()
    }

    pub fn set_model(&self, model: &impl IsA<gio::ListModel>) {
        self.imp().icons_set_view.set_model(model);
    }

    pub fn set_open_project(&self, project: Project) {
        let imp = self.imp();
        assert_eq!(project.project_type(), ProjectType::IconsSet);
        let set = project.set().unwrap();
        imp.revealer.set_reveal_child(true);
        set.connect_category_parsed(clone!(@strong self as this => move |set| {
            this.set_view(View::Icons);
            let total = set.n_parsed_categories();
            let n_categories = set.total_categories();
            let fraction = total as f64 / n_categories as f64;
            if fraction == 1.0 {
                this.imp().revealer.set_reveal_child(false);
            }
            this.imp().progress.set_fraction(fraction);
        }));
        imp.icons_set_view.load(&project);
        imp.open_project.replace(Some(project));
    }

    pub fn set_view(&self, view: View) {
        let imp = self.imp();
        imp.current_view.set(view);

        match view {
            View::Empty => {
                imp.stack.set_visible_child_name("empty");
            }
            View::Icons => {
                imp.stack.set_visible_child_name("icons");
            }
        };
    }

    fn filter(&self, search_text: Option<&str>) {
        let imp = self.imp();
        imp.icons_set_view.filter(search_text);
        if imp.icons_set_view.n_visible() == 0 {
            self.set_view(View::Empty);
        } else {
            self.set_view(View::Icons);
        }
    }

    #[template_callback]
    fn search_entry_started(&self, entry: &gtk::SearchEntry) {
        entry.grab_focus();
    }

    #[template_callback]
    fn search_entry_changed(&self, entry: &gtk::SearchEntry) {
        let search_text = entry.text().to_ascii_lowercase();
        if search_text.is_empty() {
            self.filter(None);
        } else {
            self.filter(Some(&search_text));
        }
    }

    #[template_callback]
    fn search_entry_stopped(&self, entry: &gtk::SearchEntry) {
        entry.set_text("");
    }
}
