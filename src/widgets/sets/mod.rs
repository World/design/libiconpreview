mod group;
mod page;
mod view;

pub use page::{IconsSetPage, View};
pub use view::IconsSetView;
