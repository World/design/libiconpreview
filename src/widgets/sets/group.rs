use adw::prelude::*;
use gtk::{glib, subclass::prelude::*};

use crate::{
    icons::{Category, Icon},
    utils::make_ascii_titlecase,
    widgets::{export::ExportDialog, icons::IconWidget},
};

mod imp {
    use std::cell::OnceCell;

    use super::*;

    #[derive(Debug, Default, gtk::CompositeTemplate, glib::Properties)]
    #[template(file = "../../../data/ui/icons_group.ui")]
    #[properties(wrapper_type = super::IconsGroupWidget)]
    pub struct IconsGroupWidget {
        #[template_child]
        pub(super) flowbox: TemplateChild<gtk::FlowBox>,
        #[template_child]
        pub(super) name_label: TemplateChild<gtk::Label>,
        #[property(get, set, construct_only)]
        pub(super) category: OnceCell<Category>,
        #[template_child]
        pub system_icons_label: TemplateChild<gtk::Label>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for IconsGroupWidget {
        const NAME: &'static str = "IconsGroupWidget";
        type Type = super::IconsGroupWidget;
        type ParentType = gtk::ListBoxRow;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
            klass.bind_template_instance_callbacks();
            klass.set_css_name("icons-group");
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    #[glib::derived_properties]
    impl ObjectImpl for IconsGroupWidget {
        fn constructed(&self) {
            self.parent_constructed();
            let obj = self.obj();
            let category = obj.category();
            let mut category_title = category.name();
            if category_title.len() <= 3 {
                category_title = category_title.to_uppercase();
            } else {
                make_ascii_titlecase(&mut category_title);
            };
            self.name_label.set_label(&category_title);

            let sorter = gtk::StringSorter::new(Some(Icon::this_expression("name")));
            let sort_model = gtk::SortListModel::new(Some(category.icons()), Some(sorter));
            self.flowbox.bind_model(Some(&sort_model), move |item| {
                let icon = item.clone().downcast::<Icon>().unwrap();
                IconWidget::new(icon).upcast()
            });

            if category.is_system() {
                self.system_icons_label.set_visible(true);
            }
        }
    }
    impl WidgetImpl for IconsGroupWidget {}
    impl ListBoxRowImpl for IconsGroupWidget {}
}

glib::wrapper! {
    pub struct IconsGroupWidget(ObjectSubclass<imp::IconsGroupWidget>)
        @extends gtk::Widget, gtk::ListBoxRow;
}

#[gtk::template_callbacks]
impl IconsGroupWidget {
    pub fn new(category: &Category) -> Self {
        glib::Object::builder()
            .property("category", category)
            .build()
    }

    pub(crate) fn filter(&self, search_text: Option<&str>) {
        let imp = self.imp();
        if let Some(search_text) = search_text {
            let search_text = search_text.to_owned();
            imp.flowbox.set_filter_func(move |row| {
                let icon_widget = row.downcast_ref::<IconWidget>().unwrap();
                icon_widget
                    .icon()
                    .map(|icon| icon.should_display(&search_text))
                    .unwrap_or_default()
            });
        } else {
            imp.flowbox.unset_filter_func();
        }
    }

    pub fn n_visible(&self) -> u32 {
        let mut index = 0;
        let mut counter = 0;
        while let Some(child) = self.imp().flowbox.child_at_index(index) {
            index += 1;
            // We use is_child_visible since is_visible and get_visible
            // always return true.
            if child.is_child_visible() {
                counter += 1;
            }
        }
        counter
    }

    #[template_callback]
    fn on_child_activated(&self, child: &IconWidget) {
        let Some(icon) = child.icon() else {
            return;
        };
        let export_dialog = ExportDialog::new(icon);
        let window = self.root().and_downcast::<gtk::Window>().unwrap();

        export_dialog.present(&window);
    }
}
