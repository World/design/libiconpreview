use gtk::{
    gio,
    glib::{self, clone},
    prelude::*,
    subclass::prelude::*,
};

use super::group::IconsGroupWidget;
use crate::{icons::Category, Project};
mod imp {
    use super::*;

    #[derive(Debug, Default)]
    pub struct IconsSetView {
        pub(super) listbox: gtk::ListBox,
        pub(super) filter_model: gtk::FilterListModel,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for IconsSetView {
        const NAME: &'static str = "IconsSetView";
        type Type = super::IconsSetView;
        type ParentType = gtk::Widget;

        fn class_init(klass: &mut Self::Class) {
            klass.set_layout_manager_type::<gtk::BinLayout>();
            klass.set_css_name("icons-set-view");
        }
    }

    impl ObjectImpl for IconsSetView {
        fn constructed(&self) {
            self.parent_constructed();
            self.listbox.set_parent(&*self.obj());

            let filter = gtk::CustomFilter::new(|a| {
                let category = a.downcast_ref::<Category>().unwrap();
                category.is_rendered()
            });

            self.filter_model.set_filter(Some(&filter));
            let multi_sorter = gtk::MultiSorter::new();
            multi_sorter.append(gtk::StringSorter::new(Some(Category::this_expression(
                "name",
            ))));
            multi_sorter.append(gtk::CustomSorter::new(|a, b| {
                let a = a.downcast_ref::<Category>().unwrap();
                let b = b.downcast_ref::<Category>().unwrap();
                match (a.is_system(), b.is_system()) {
                    (true, true) | (false, false) => gtk::Ordering::Equal,
                    (true, false) => gtk::Ordering::Larger,
                    (false, true) => gtk::Ordering::Smaller,
                }
            }));
            let sort_model =
                gtk::SortListModel::new(Some(self.filter_model.clone()), Some(multi_sorter));
            self.listbox.bind_model(Some(&sort_model), move |obj| {
                let category = obj.downcast_ref::<Category>().unwrap();
                IconsGroupWidget::new(category).upcast()
            });
        }

        fn dispose(&self) {
            self.listbox.unparent();
        }
    }
    impl WidgetImpl for IconsSetView {}
}

glib::wrapper! {
    pub struct IconsSetView(ObjectSubclass<imp::IconsSetView>) @extends gtk::Widget, gtk::Box;
}

impl IconsSetView {
    pub fn set_model(&self, model: &impl IsA<gio::ListModel>) {
        self.imp().filter_model.set_model(Some(model));
    }

    pub fn filter(&self, search_text: Option<&str>) {
        let imp = self.imp();
        if let Some(search_text) = search_text {
            let search_text = search_text.to_owned();
            imp.listbox.set_filter_func(move |item| {
                let group_widget = item.downcast_ref::<IconsGroupWidget>().unwrap();
                let group = group_widget.category();
                if group.name().to_ascii_lowercase().contains(&search_text) {
                    group_widget.filter(None);
                    true
                } else {
                    group_widget.filter(Some(&search_text));
                    group_widget.n_visible() > 0
                }
            });
        } else {
            imp.listbox.unset_filter_func();
            let mut index = 0;
            while let Some(child) = imp.listbox.row_at_index(index) {
                let group_widget = child.downcast_ref::<IconsGroupWidget>().unwrap();
                group_widget.filter(None);
                index += 1;
            }
        }
    }

    pub fn n_visible(&self) -> u32 {
        let mut index = 0;
        let mut counter = 0;
        while let Some(child) = self.imp().listbox.row_at_index(index) {
            index += 1;
            // We use is_child_visible since is_visible and get_visible
            // always return true.
            if child.is_child_visible() {
                counter += 1;
            }
        }
        counter
    }

    pub fn load(&self, project: &Project) {
        let filter = self.imp().filter_model.filter().unwrap();
        let set = project.set().unwrap();
        set.connect_category_parsed(clone!(@strong filter, @strong project => move |_| {
            filter.changed(gtk::FilterChange::Different);
        }));
        self.set_model(&set);
    }
}
